export class StringHelpers {
  /**
   * Insere zero à esquerda
   * para números com apenas
   * um dígito
   * @param {number} numero Número a ser formatado
   */
  static leftPad(numero) {
    const numeroString = numero.toString();

    if (numeroString.length === 1) {
      return '0' + numeroString;
    }
    return numeroString;
  }
}
